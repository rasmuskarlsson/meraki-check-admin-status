import requests
import dotenv
import os

dotenv.load_dotenv()

# Global variables
apikey = os.getenv("APIKEY")
got_admin = False

if __name__ == "__main__":
    print("Type the e-mail of the administrator you want to check has a verified account")
    administrator_to_check = input()

    url = "https://api.meraki.com/api/v1/organizations"
    headers = {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "X-Cisco-Meraki-API-Key": f"{apikey}"
    }

    get_organizations = requests.get(url, headers=headers, data=None).json()

    for organization in get_organizations:
        print(f"Checking organization {organization['name']}")
        if got_admin == False:
            if organization["api"]["enabled"] == True:
                url_admin = f"https://api.meraki.com/api/v1/organizations/{organization['id']}/admins"
                get_admins = requests.get(url_admin, headers=headers).json()
                for admin in get_admins:
                    if admin["email"] == administrator_to_check:
                        print(f"The selected email, {administrator_to_check}, has the following status: {admin['accountStatus']}")
                        print(f"Administrator found in {organization['name']}\n")
                        got_admin = True
            else:
                print(f"Organization {organization['name']} has not activated API\n")
        else:
            break
